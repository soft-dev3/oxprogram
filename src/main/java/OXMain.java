
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class OXMain {

    static char board[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showBoard();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    private static void showBoard() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(board[r][c] + " ");
            }
            System.out.println();
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    private static boolean inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();
        if (row > 0 && col > 0 && row <= 3 && col <= 3) { //&& board[row - 1][col - 1] == '-'
            return true;
        } else {
            showErrorRowCol();
        }
        return false;
    }

    private static void showErrorRowCol() {
        System.out.println("***Please input row, col again***");
        showBoard();
        showTurn();
        inputRowCol();
    }

    private static void process() {
        if (setBoard()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw()) {
                showDraw();
                return;
            }
        }
        //
        else{
            inputRowCol();
            process();
        }
        count++;
        switchPlayer();
    }

    private static void showDraw() {
        finish = true;
        showBoard();
        System.out.println(">>>  Draw <<<");
    }

    private static void showWin() {
        showBoard();
        System.out.println(">>> " + currentPlayer + " Win <<<");
    }

    private static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    private static boolean setBoard() {
        if (board[row - 1][col - 1] != '-') {
            System.out.println("***Error***");
            return false;
        } else {
            board[row - 1][col - 1] = currentPlayer;
            return true;
        }

    }

    private static boolean checkWin() {
        if (checkCol()) {
            return true;
        } else if (checkRow()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    private static boolean checkCol() {
        for (int r = 0; r < 3; r++) {
            if (board[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkRow() {
        for (int c = 0; c < 3; c++) {
            if (board[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal() {
        if (checkDia1()) {
            return true;
        } else if (checkDia2()) {
            return true;
        }
        return false;
    }

    private static boolean checkDia1() {
        for (int i = 0; i < 3; i++) {
            if (board[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDia2() {
        for (int i = 0; i < 3; i++) {
            if (board[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }
}
